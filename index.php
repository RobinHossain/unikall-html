<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css">

    <title>Hello, world!</title>
</head>
<body>

<section class="header">
    <div class="logo">
        <img src="assets/images/logo.png" alt="Logo"/>
        <span class="logo_text">
            UNIKALL<br>
            <small>Centre for Social Impact</small>
        </span>
    </div>
    <div class="nav_menu">
        <ul class="top_nav">
            <li><a href="#">Home</a> </li>
            <li><a href="#">Our Story</a></li>
            <li><a href="#">Results</a></li>
            <li><a href="#">Campaigns</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">Login</a></li>
        </ul>
    </div>
</section>



<section class="act_love">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="slogan large art">
                   Acts of <span>love</span><br> have the <span>power</span><br> to make a <span>Difference</span>
                </div>
                <div class="slogan small normal">
                    <p>We help you to create an impact through education, health and social businesses</p>
                </div>
                <div class="button btn round blue">
                    Discover
                </div>
            </div>
            <div class="col-md-7">
                <div class="super_boy">
                    <img class="boy_image responsive" src="assets/images/gilr.png" alt="Super Boy">
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>